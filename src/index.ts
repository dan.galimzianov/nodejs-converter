import {Command} from 'commander';
import {existsSync, mkdirSync} from 'fs';
import isImage from 'is-image';
import {resolve} from 'path';
import filesHandler from './handlers/files-handler';
import imageHandler from './handlers/image-handler';
import getFilesPaths from './utils/get-files-paths';

const {target, output, config} = new Command()
  .requiredOption('--target <char>')
  .requiredOption('--output <char>')
  .parse()
  .opts<{target: string; output: string; config: string}>();

const TARGET_DIRECTORY = resolve(target);
const OUTPUT_DIRECTORY = resolve(output);

async function main() {
  if (!existsSync(TARGET_DIRECTORY)) {
    console.error('No target dir');
    return;
  }

  if (!existsSync(OUTPUT_DIRECTORY)) {
    mkdirSync(OUTPUT_DIRECTORY);
  }

  console.time('Script work');
  const paths = await getFilesPaths(TARGET_DIRECTORY);

  (
    await Promise.all(
      paths.map(async filePath => ({
        handlerRes: isImage(filePath)
          ? await imageHandler(filePath, OUTPUT_DIRECTORY)
          : await filesHandler(filePath, OUTPUT_DIRECTORY),
        filePath,
      }))
    )
  ).map(({handlerRes, filePath}) => console.log(filePath, handlerRes.status));
  console.timeEnd('Script work');
}

main();
