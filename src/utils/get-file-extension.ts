export default function getFileExtension(filename: string) {
  return filename.split('.').filter(Boolean).slice(1).join('.');
}
