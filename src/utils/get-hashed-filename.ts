import {MD5} from 'crypto-js';
import getFileExtension from './get-file-extension';

export default function getHashedFilename(filePath: string) {
  return `${MD5(filePath).toString()}.${getFileExtension(filePath)}`;
}
