import * as child_process from 'child_process';
import * as util from 'util';

const exec = util.promisify(child_process.exec);

// не очень кроссплатформенно, зато коротко :)
export default async function getFilesPaths(
  rootDir: string
): Promise<string[]> {
  return (await exec(`find ${rootDir} -type f`)).stdout.trim().split('\n');
}
// git clone https://
// git remote add origin https://oauth2:glpat-NWZvrhgDy51arMDed7p1@gitlab.com:dan.galimzianov/nodejs-converter.git
