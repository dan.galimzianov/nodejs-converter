import * as path from 'path';
import getFileExtension from './get-file-extension';

export default function changeExtension(file: string, extension: string) {
  const basename = path.basename(file, getFileExtension(file));
  return path.join(path.dirname(file), basename + extension);
}
