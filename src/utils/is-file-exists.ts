import {constants} from 'fs';
import {access} from 'fs/promises';

export default async function isFileExists(filepath: string) {
  try {
    await access(filepath, constants.F_OK);
    return true;
  } catch {
    return false;
  }
}
