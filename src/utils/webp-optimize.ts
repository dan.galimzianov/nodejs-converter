import sharp from 'sharp';

export default async function webpOptimize(
  sourceFilePath: string,
  targetPath: string
) {
  await sharp(sourceFilePath).webp().toFile(targetPath);
}
