export enum HandlerResultStatus {
  CACHED = 'CACHED',
  MODIFIED = 'MODIFIED',
  ERROR = 'ERROR',
}

export interface HandlerResult {
  status: HandlerResultStatus;
  error?: any;
}
