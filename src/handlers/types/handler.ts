import {HandlerResult} from './handler-result-status';

export type Handler<Options extends {} = {}> = (
  filePath,
  outputDir,
  options?: Options
) => Promise<HandlerResult> | HandlerResult;
