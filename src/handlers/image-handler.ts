import changeExtension from '../utils/change-extension';
import getHashedFilename from '../utils/get-hashed-filename';
import isFileExists from '../utils/is-file-exists';
import webpOptimize from '../utils/webp-optimize';
import {Handler} from './types/handler';
import {
  HandlerResultStatus,
  HandlerResult,
} from './types/handler-result-status';

export default (async function imageHandler(
  filePath,
  outputDir
): Promise<HandlerResult> {
  try {
    const hashedFilename = getHashedFilename(filePath);
    const targetFilepath = changeExtension(
      `${outputDir}/${hashedFilename}`,
      'webp'
    );

    if (await isFileExists(targetFilepath)) {
      return {status: HandlerResultStatus.CACHED};
    }

    await webpOptimize(filePath, targetFilepath);
    return {status: HandlerResultStatus.MODIFIED};
  } catch (e) {
    return {status: HandlerResultStatus.ERROR, error: e};
  }
} as Handler);
