import {copyFile} from 'fs/promises';
import getHashedFilename from '../utils/get-hashed-filename';
import isFileExists from '../utils/is-file-exists';
import {Handler} from './types/handler';
import {HandlerResultStatus} from './types/handler-result-status';

export default (async function filesHandler(filePath, outputDir) {
  try {
    const hashedFilename = getHashedFilename(filePath);
    const outputFilename = `${outputDir}/${hashedFilename}`;
    if (await isFileExists(outputFilename)) {
      return {status: HandlerResultStatus.CACHED};
    }

    await copyFile(filePath, outputFilename);
    return {status: HandlerResultStatus.MODIFIED};
  } catch (e) {
    return {status: HandlerResultStatus.ERROR, error: e};
  }
} as Handler);
